/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.loader.eurostat;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code.DEU;
import static com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code.ENG;
import static com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code.FRA;
import static com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code.POR;
import static com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code.SPA;
import static com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2EntryType.CLASS;
import static com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2EntryType.DIVISION;
import static com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2EntryType.GROUP;
import static com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2EntryType.SECTION;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Stream.of;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.EntryDataLoader;
import com.tinubu.commons.referential.EntryDataLoadingException;
import com.tinubu.commons.referential.TranslatableName;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2Data;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2EntryType;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2Code;

/**
 * NACE Rev.2 {@link EntryDataLoader} from data files from Eurostat.
 */
public class EurostatNaceRev2ReferentialLoader implements EntryDataLoader<NaceRev2Data> {

   private static final String XML_RESOURCE_PATH = "resources/nace_rev2-%s.xml";
   private static final Iso6392Alpha3Code[] XML_RESOURCE_LANGUAGES = { ENG, FRA, DEU, SPA, POR };

   private final Iso6392Alpha3Code[] languages;

   public EurostatNaceRev2ReferentialLoader(Iso6392Alpha3Code... languages) {
      this.languages = validateLanguages(languages);
   }

   public EurostatNaceRev2ReferentialLoader() {
      this(XML_RESOURCE_LANGUAGES);
   }

   @Override
   public List<NaceRev2Data> loadEntryDatas() {
      try {
         return mergeLanguages(languages).stream().map(NaceRev2Data.class::cast).collect(toList());
      } catch (EntryDataLoadingException e) {
         throw e;
      } catch (Exception e) {
         throw new EntryDataLoadingException(e.getMessage(), e);
      }
   }

   @Override
   public boolean hasChanged() {
      return false;
   }

   private Iso6392Alpha3Code[] validateLanguages(Iso6392Alpha3Code[] languages) {
      notNull(languages, "languages");
      satisfies(languages.length, length -> length > 0, "languages", "length must be > 0");
      satisfies(languages,
                __ -> Stream.of(languages).allMatch(language -> Arrays.asList(XML_RESOURCE_LANGUAGES).contains(language)),
                "languages",
                String.format("must be in %s",
                              Stream
                                    .of(XML_RESOURCE_LANGUAGES)
                                    .map(Iso6392Alpha3Code::entryCode)
                                    .collect(joining(",", "[", "]"))));

      return languages;
   }

   private static String documentName(Iso6392Alpha3Code language) {
      return String.format(XML_RESOURCE_PATH, language.entryCode());
   }

   private static List<NaceData> mergeLanguages(Iso6392Alpha3Code... languages) {
      satisfies(languages.length, length -> length > 0, "languages", "length must be > 0");

      Iso6392Alpha3Code primaryLanguage = languages[0];
      List<Iso6392Alpha3Code> extraLanguages = Stream.of(languages).skip(1).collect(toList());

      Map<Iso6392Alpha3Code, Map<String, NaceXmlEntry>> xmlData = //
            of(languages).collect(toMap(identity(),
                                        language -> readFile(documentName(language))
                                              .stream()
                                              .collect(toMap(NaceXmlEntry::id, identity()))));

      return xmlData.get(primaryLanguage).values().stream().map(xmlEntry -> {
         DefaultTranslatableNameBuilder name = new DefaultTranslatableNameBuilder();

         name.name(primaryLanguage, xmlEntry.label());
         for (Iso6392Alpha3Code extraLanguage : extraLanguages) {
            name.name(extraLanguage,
                      nullable(xmlData.get(extraLanguage))
                            .flatMap(elm -> nullable(elm.get(xmlEntry.id())).map(NaceXmlEntry::label))
                            .orElseThrow(() -> new IllegalStateException(String.format(
                                  "Unknown '%s' language for '%s' activity",
                                  extraLanguage.entryCode(),
                                  xmlEntry.id()))));
         }

         return new NaceData(xmlEntry.id(), xmlEntry.section(), name.build(), xmlEntry.level());
      }).collect(toList());
   }

   private static List<NaceXmlEntry> readFile(String documentName) {
      Document doc = xmlDocument(documentName).orElseThrow(() -> new IllegalStateException(String.format(
            "Unknown '%s' document",
            documentName)));
      Element root = doc.getRootElement();
      Element classification = root.getChild("Classification");
      List<Element> items = classification.getChildren("Item");
      String currentSection = null;
      List<NaceXmlEntry> nomenclature = new ArrayList<>();
      for (Element item : items) {
         Attribute id = item.getAttribute("id");
         String idItem = id.getValue();
         Attribute idLevel = item.getAttribute("idLevel");
         int idLevelItem = Integer.parseInt(idLevel.getValue());
         Element label = item.getChild("Label");
         Element labelText = label.getChild("LabelText");
         String labelTextItem = labelText.getValue();
         if (idLevelItem == 1) {
            currentSection = idItem;
         }
         nomenclature.add(new NaceXmlEntry(idItem, currentSection, labelTextItem, idLevelItem));
      }
      return nomenclature;
   }

   private static Optional<Document> xmlDocument(String documentName) {
      try (InputStream stream = EurostatNaceRev2ReferentialLoader.class.getResourceAsStream(documentName)) {
         if (stream == null) {
            return optional();
         } else {
            return optional(new SAXBuilder().build(stream));
         }
      } catch (JDOMException | IOException e) {
         throw new IllegalStateException(String.format("Error during '%s' parse", documentName), e);
      }
   }

   private static class NaceXmlEntry {
      private final String label;
      private final String id;
      private final String section;
      private final int level;

      public NaceXmlEntry(String id, String book, String label, int level) {
         this.id = id;
         this.label = label;
         this.section = book;
         this.level = level;
      }

      public String label() {
         return label;
      }

      public String id() {
         return id;
      }

      public String section() {
         return section;
      }

      public int level() {
         return level;
      }
   }

   private static class NaceData implements NaceRev2Data {
      private final TranslatableName name;
      private final String id;
      private final String section;
      private final int level;
      private final String division;
      private final String group;
      private final String naceClass;

      public NaceData(String id, String section, TranslatableName name, int level) {
         this.id = notBlank(id, "id");
         this.name = name;
         this.section = section;
         this.level = level;

         switch (level) {
            case 1:
               division = null;
               group = null;
               naceClass = null;
               break;
            case 2:
               division = id;
               group = null;
               naceClass = null;
               break;
            case 3:
               division = id.substring(0, 2);
               group = id.substring(0, 4);
               naceClass = null;
               break;
            case 4:
               division = id.substring(0, 2);
               group = id.substring(0, 4);
               naceClass = id.substring(0, 5);
               break;
            default:
               throw new IllegalArgumentException(String.format("Unknown '%d' NACE Rev2 level", level));
         }
      }

      @Override
      public NaceRev2Code section() {
         return NaceRev2Code.of(section);
      }

      @Override
      public Optional<NaceRev2Code> division() {
         return nullable(division).map(NaceRev2Code::of);
      }

      @Override
      public Optional<NaceRev2Code> group() {
         return nullable(group).map(NaceRev2Code::of);
      }

      @Override
      public Optional<NaceRev2Code> naceClass() {
         return nullable(naceClass).map(NaceRev2Code::of);
      }

      @Override
      public TranslatableName name() {
         return name;
      }

      @Override
      public NaceRev2EntryType type() {
         switch (level) {
            case 1:
               return SECTION;
            case 2:
               return DIVISION;
            case 3:
               return GROUP;
            case 4:
               return CLASS;
            default:
               throw new IllegalStateException(String.format("Unknown '%d' NACE Rev2 level", level));
         }
      }

      @Override
      public NaceRev2Code naceRev2() {
         return NaceRev2Code.of(id);
      }

   }

}
