/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials;

import org.junit.jupiter.api.extension.ExtendWith;

import com.tinubu.commons.referential.Entry;
import com.tinubu.commons.referential.Referential;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.ReferentialProvider;
import com.tinubu.commons.test.fixture.junit.FixtureConfig;
import com.tinubu.commons.test.fixture.junit.FixtureExtension;

@ExtendWith(FixtureExtension.class)
@FixtureConfig(basePackages = "com.tinubu.commons.referential.referentials.fixture")
public class AbstractReferentialTest {

   protected static <T extends Entry<T>> ReferentialProvider referentialProvider(final Referential<T> referential) {
      return new ReferentialProvider() {
         @Override
         public ReferentialCode<? extends Entry<?>, ? extends Referential<?>> referentialCode() {
            return referential.referentialCode();
         }

         @Override
         @SuppressWarnings("unchecked")
         public Class<? extends Referential<?>> referentialClass() {
            return (Class<? extends Referential<?>>) referential.getClass();
         }

         @Override
         public Referential<?> build() {
            return referential;
         }
      };
   }

}
