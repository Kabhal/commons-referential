/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Objects;
import java.util.StringJoiner;

import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;

public class TestData implements EntryData {

   private final EntryCode entryCode;
   private final TranslatableName entryName;

   private TestData(TestDataBuilder builder) {
      this.entryCode = notNull(builder.entryCode, "entryCode");
      this.entryName = notNull(builder.entryName, "entryName");
   }

   public EntryCode entryCode() {
      return entryCode;
   }

   public TranslatableName entryName() {
      return entryName;
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", TestData.class.getSimpleName() + "[", "]")
            .add("entryCode='" + entryCode + "'")
            .add("entryName='" + entryName + "'")
            .toString();
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      TestData that = (TestData) o;
      return Objects.equals(entryCode, that.entryCode) && Objects.equals(entryName, that.entryName);
   }

   @Override
   public int hashCode() {
      return Objects.hash(entryCode, entryName);
   }

   public static class TestDataBuilder {

      private EntryCode entryCode;
      private TranslatableName entryName;

      public TestDataBuilder entryCode(EntryCode entryCode) {
         this.entryCode = entryCode;
         return this;
      }

      public TestDataBuilder entryName(Iso6392Alpha3Code language, String entryName) {
         this.entryName = new DefaultTranslatableNameBuilder().name(language, entryName).build();
         return this;
      }

      public TestDataBuilder entryName(String entryName) {
         return entryName(Iso6392Alpha3Code.ENG, entryName);
      }

      public TestDataBuilder entryCode(String entryCode) {
         return entryCode(EntryCode.of(entryCode));
      }

      public TestData build() {
         return new TestData(this);
      }
   }
}
