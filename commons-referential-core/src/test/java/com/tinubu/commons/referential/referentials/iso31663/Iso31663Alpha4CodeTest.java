/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31663;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.Test;

class Iso31663Alpha4CodeTest {

   @Test
   public void testOfWhenCaseInsensitive() {
      assertThat(Iso31663Alpha4Code.of("aidj")).isEqualTo(Iso31663Alpha4Code.AIDJ);
      assertThat(Iso31663Alpha4Code.of("AIDJ")).isEqualTo(Iso31663Alpha4Code.AIDJ);
   }

   @Test
   public void testOfWhenNominal() {
      assertThat(Iso31663Alpha4Code.of("AIDJ").entryCode()).isEqualTo("AIDJ");
   }

   @Test
   public void testOfWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> Iso31663Alpha4Code.of(null)).withMessage("'entryCode' must not be null");
      assertThatIllegalArgumentException().isThrownBy(() -> Iso31663Alpha4Code.of("")).withMessage("'entryCode' must not be blank");
   }

}