/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.referential.Referentials.clearRegisteredReferentials;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.ReferentialsTest.FailingReferential.FailingReferentialProvider;
import com.tinubu.commons.referential.TestData.TestDataBuilder;
import com.tinubu.commons.referential.TestReferential.TestReferentialBuilder;

public class ReferentialsTest {

   @BeforeAll
   public static void registerTestReferentials() {
      clearRegisteredReferentials();

      List<TestData> test2Loader =
            Arrays.asList(new TestDataBuilder().entryCode("FR").entryName("France").build(),
                          new TestDataBuilder().entryCode("USXX").entryName("USA").build());

      Referentials.registerReferential(new TestReferentialBuilder());
      Referentials.registerReferential(new TestReferentialBuilder()
                                             .referentialCode(ReferentialCode.of("TEST2"))
                                             .referentialLoader(new EntryDataLoader<TestData>() {
                                                @Override
                                                public List<TestData> loadEntryDatas() {
                                                   return test2Loader;
                                                }

                                                @Override
                                                public boolean hasChanged() {
                                                   return false;
                                                }
                                             }));
      Referentials.registerReferential(new FailingReferentialProvider());
   }

   @Nested
   public class InstantiateReferential {

      @Test
      public void testInstantiateReferential() {

         Referential<?> referential = Referentials.referential(ReferentialCode.untypedOf("TEST"));

         assertThat(referential).isNotNull();
         assertThat(referential).isInstanceOf(TestReferential.class);

         assertThat(Referentials.referential(ReferentialCode.untypedOf("TEST")))
               .as("Subsequent instantiations must return the same singleton instance")
               .isEqualTo(referential);
      }

      @Test
      public void testInstantiateReferentialWhenBadParameters() {
         assertThatNullPointerException().isThrownBy(() -> Referentials.referential(null)).withMessage("'referentialCode' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> Referentials.referential(ReferentialCode.untypedOf("")))
               .withMessage("'referentialCode' must not be blank");
      }

      @Test
      public void testInstantiateReferentialWhenNotFound() {

         assertThatExceptionOfType(UnknownReferentialException.class)
               .isThrownBy(() -> Referentials.referential(ReferentialCode.untypedOf("UNREGISTERED")))
               .withMessage("Not registered 'UNREGISTERED' referential");
      }

      @Test
      public void testInstantiateReferentialWhenLoadingFailure() {
         assertThatExceptionOfType(ReferentialInitializationException.class)
               .isThrownBy(() -> Referentials.referential(ReferentialCode.untypedOf("FAILING")))
               .withMessage("Can't load 'FAILING' referential of type 'FailingReferential'");
      }

      @Test
      public void testInstantiateReferentialSingletonWhenDifferentInstantiationMethods() {
         Referential<?> referential = Referentials.referential(ReferentialCode.untypedOf("TEST"));

         assertThat(Referentials.referential(ReferentialCode.untypedOf("TEST")))
               .as("Subsequent instantiations must return the same singleton instance")
               .isEqualTo(referential);
      }

   }

   @Nested
   public class RegisterReferential {

      @Test
      public void testRegisterReferential() {
         TestReferentialBuilder referentialProvider = new TestReferentialBuilder() {
            @Override
            public ReferentialCode<TestEntry, TestReferential> referentialCode() {
               return ReferentialCode.of("TEST2");
            }
         };
         Referentials.registerReferential(referentialProvider);

         assertThat(Referentials.registeredReferentials()).contains(referentialProvider);
      }

      @Test
      public void testRegisterReferentialWhenAlreadyRegisteredWithSameName() {
         assertThat(Referentials.registeredReferentials()).extracting(ReferentialProvider::referentialCode).contains(ReferentialCode.of("TEST"));

         TestReferentialBuilder referentialProvider = new TestReferentialBuilder();
         Referentials.registerReferential(referentialProvider);

         assertThat(Referentials.registeredReferentials()).contains(referentialProvider);
      }
   }

   @Nested
   public class SearchEntry {

      @Test
      public void testEntryWhenNominal() {
         Optional<Entry<?>> entry = Referentials.entry(EntryCode.of("FR"),
                                                       ReferentialCode.of("TEST"),
                                                       ReferentialCode.of("TEST2"));

         Assertions.assertThat(entry).isPresent();
         Assertions.assertThat(entry).get().extracting(Entry::entryCode).isEqualTo(EntryCode.of("FR"));
      }

      @Test
      public void testEntryWhenBlankEntryCode() {
         assertThatNullPointerException()
               .isThrownBy(() -> Referentials.entry(null,
                                                    ReferentialCode.of("TEST"),
                                                    ReferentialCode.of("TEST2")))
               .withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> Referentials.entry(EntryCode.of(""),
                                                    ReferentialCode.of("TEST"),
                                                    ReferentialCode.of("TEST2")))
               .withMessage("'entryCode' must not be blank");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> Referentials.entry(EntryCode.of(" "),
                                                    ReferentialCode.of("TEST"),
                                                    ReferentialCode.of("TEST2")))
               .withMessage("'entryCode' must not be blank");
      }

      @Test
      public void testEntryWhenEmptyReferentials() {
         assertThatNullPointerException()
               .isThrownBy(() -> Referentials.entry(EntryCode.of("FR"), (ReferentialCode<?, ?>[]) null))
               .withMessage("'referentialCodes' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> Referentials.entry(EntryCode.of("FR"), ReferentialCode.of("TEST"), null))
               .withMessage("'referentialCodes' must not have null elements at index : 1");
         assertThatIllegalArgumentException().isThrownBy(() -> Referentials.entry(EntryCode.of("FR"))).withMessage("'referentialCodes' must not be empty");
      }

      @Test
      public void testEntryWhenSameEntryCodeInSeveralReferentials() {
         Optional<Entry<?>> entry = Referentials.entry(EntryCode.of("FR"),
                                                       ReferentialCode.of("TEST"),
                                                       ReferentialCode.of("TEST2"));

         Assertions.assertThat(entry).isPresent();
         Assertions.assertThat(entry).get().satisfies(c -> {
            assertThat(c.referential()).isEqualTo(Referentials.referential(ReferentialCode.untypedOf("TEST")));
            assertThat(c.entryCode()).isEqualTo(EntryCode.of("FR"));
         });
      }

      @Test
      public void testEntryWhenCodeInLastReferential() {
         Optional<Entry<?>> entry = Referentials.entry(EntryCode.of("USXX"),
                                                       ReferentialCode.of("TEST"),
                                                       ReferentialCode.of("TEST2"));

         Assertions.assertThat(entry).isPresent();
         Assertions.assertThat(entry).get().satisfies(c -> {
            assertThat(c.referential()).isEqualTo(Referentials.referential(ReferentialCode.untypedOf("TEST2")));
            assertThat(c.entryCode()).isEqualTo(EntryCode.of("USXX"));
         });
      }

      @Test
      public void testEntryWhenCodeNotFound() {
         Optional<Entry<?>> entry = Referentials.entry(EntryCode.of("ZZ"),
                                                       ReferentialCode.of("TEST"),
                                                       ReferentialCode.of("TEST2"));

         Assertions.assertThat(entry).isEmpty();
      }

   }

   public static class FailingReferential implements Referential<TestEntry> {

      public FailingReferential() {
         throw new IllegalStateException("Testing initialization failure");
      }

      @Override
      public ReferentialCode<TestEntry, FailingReferential> referentialCode() {
         return ReferentialCode.of("FAILING");
      }

      @Override
      public String displayName() {
         return "FAILING";
      }

      @Override
      public List<TestEntry> entries() {
         return null;
      }

      @Override
      public Optional<TestEntry> optionalEntry(EntryCode entryCode) {
         return Optional.empty();
      }

      @Override
      public TestEntry entry(EntryCode entryCode) {
         return null;
      }

      public static class FailingReferentialProvider implements ReferentialProvider {

         @Override
         public ReferentialCode<TestEntry, FailingReferential> referentialCode() {
            return ReferentialCode.of("FAILING");
         }

         @Override
         public Class<FailingReferential> referentialClass() {
            return FailingReferential.class;
         }

         @Override
         public FailingReferential build() {
            return new FailingReferential();
         }
      }
   }

}