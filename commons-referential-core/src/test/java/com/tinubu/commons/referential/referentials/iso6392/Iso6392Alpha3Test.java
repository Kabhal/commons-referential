/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso6392;

import static com.tinubu.commons.referential.Referentials.clearRegisteredReferentials;
import static com.tinubu.commons.referential.referentials.IsoReferentials.iso6392Alpha3;
import static com.tinubu.commons.referential.referentials.IsoReferentials.iso6392Alpha3Referential;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.Mockito.doReturn;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.Entry;
import com.tinubu.commons.referential.EntryCode;
import com.tinubu.commons.referential.ReferentialCode;
import com.tinubu.commons.referential.Referentials;
import com.tinubu.commons.referential.UnknownEntryException;
import com.tinubu.commons.referential.referentials.AbstractReferentialTest;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2Code;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2Referential;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3.Iso6392Alpha3Data;
import com.tinubu.commons.test.fixture.junit.Fixture;

public class Iso6392Alpha3Test extends AbstractReferentialTest {

   @BeforeAll
   public static void registerTestReferentials(@Fixture Iso6391Alpha2Referential iso6391Alpha2Referential,
                                               @Fixture Iso6392Alpha3Referential iso6392Alpha3Referential) {
      clearRegisteredReferentials();
      Referentials.registerReferential(referentialProvider(iso6391Alpha2Referential));
      Referentials.registerReferential(referentialProvider(iso6392Alpha3Referential));
   }

   @Test
   public void testEntryWhenNominal(@Fixture Iso6392Alpha3Data isoEntryData) {
      Iso6392Alpha3Referential referential = iso6392Alpha3Referential();
      Iso6392Alpha3 fr = new Iso6392Alpha3(referential, isoEntryData);

      assertThat(fr.entryCode()).isEqualTo(EntryCode.of("fra"));
      assertThat(fr.entryData()).isEqualTo(isoEntryData);
      assertThat(fr.referential()).isEqualTo(referential);
      assertThat(fr).isEqualTo(new Iso6392Alpha3(referential, isoEntryData));
      assertThat(fr).hasSameHashCodeAs(new Iso6392Alpha3(referential, isoEntryData));
   }

   @Test
   public void testInstantiateEntryWhenBadParameters(@Fixture Iso6392Alpha3Data isoEntryData) {
      assertThatNullPointerException()
            .isThrownBy(() -> new Iso6392Alpha3(null, isoEntryData))
            .withMessage("'referential' must not be null");

      Iso6392Alpha3Referential referential = iso6392Alpha3Referential();

      assertThatNullPointerException()
            .isThrownBy(() -> new Iso6392Alpha3(referential, null))
            .withMessage("'entryData' must not be null");
   }

   @Test
   public void testEntryMappingWhenNominal(@Fixture Iso6392Alpha3Data isoEntryData) {
      Iso6392Alpha3Referential referential = iso6392Alpha3Referential();
      Iso6392Alpha3 fr = new Iso6392Alpha3(referential, isoEntryData);

      assertThat(fr.entryMapping(ReferentialCode.<Iso6391Alpha2, Iso6391Alpha2Referential>of("ISO_6391_ALPHA2")))
            .map(Entry::entryCode)
            .hasValue(Iso6391Alpha2Code.FR);
   }

   @Test
   public void testEntryMappingWhenUnknownMappedEntry(@Fixture Iso6392Alpha3Data isoEntryData) {
      Iso6392Alpha3Referential referential = iso6392Alpha3Referential();
      doReturn(Optional.of(Iso6391Alpha2Code.of("zz")))
            .when(isoEntryData)
            .entryCode(ReferentialCode.<Iso6391Alpha2, Iso6391Alpha2Referential>of("ISO_6391_ALPHA2"));

      Iso6392Alpha3 fr = new Iso6392Alpha3(referential, isoEntryData);

      assertThatExceptionOfType(UnknownEntryException.class).isThrownBy(() -> fr.entryMapping(ReferentialCode.<Iso6391Alpha2, Iso6391Alpha2Referential>of(
            "ISO_6391_ALPHA2")));
   }

   @Test
   public void testEntryComparableWhenNominal() {
      assertThat(iso6392Alpha3("fra").compareTo(iso6392Alpha3("fra"))).isEqualTo(0);
      assertThat(iso6392Alpha3("fra").compareTo(iso6392Alpha3("zul"))).isLessThan(0);
      assertThat(iso6392Alpha3("fra").compareTo(iso6392Alpha3("aar"))).isGreaterThan(0);
   }

   @Test
   public void testEntryComparableWhenNullEntry() {
      assertThatNullPointerException()
            .isThrownBy(() -> iso6392Alpha3("fra").compareTo(null))
            .withMessage("'entry' must not be null");
   }

}
