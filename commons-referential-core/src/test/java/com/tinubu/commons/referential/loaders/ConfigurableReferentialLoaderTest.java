/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.loaders;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.TestData;
import com.tinubu.commons.referential.TestData.TestDataBuilder;
import com.tinubu.commons.referential.loaders.ConfigurableReferentialLoader.ConfigurableReferentialLoaderBuilder;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;

class ConfigurableReferentialLoaderTest {

   @Test
   public void testConfigurableReferentialLoaderWhenNominal() {
      TestData aa = new TestDataBuilder().entryName("Aa").entryCode(Iso31661Alpha2Code.of("AA")).build();
      TestData ab = new TestDataBuilder().entryName("Ab").entryCode(Iso31661Alpha2Code.of("AB")).build();
      TestData ac = new TestDataBuilder().entryName("Ac").entryCode(Iso31661Alpha2Code.of("AC")).build();
      TestData ad = new TestDataBuilder().entryName("Ad").entryCode(Iso31661Alpha2Code.of("AD")).build();
      TestData ae = new TestDataBuilder().entryName("Ae").entryCode(Iso31661Alpha2Code.of("AE")).build();

      ConfigurableReferentialLoader<TestData> build = new ConfigurableReferentialLoaderBuilder<TestData>()
            .entryDatas(aa, ab)
            .addEntryData(ac)
            .addEntryDatas(ad)
            .addEntryDatas(Collections.singletonList(ae))
            .build();

      assertThat(build.loadEntryDatas()).containsExactlyInAnyOrder(aa, ab, ac, ad, ae);
   }

}