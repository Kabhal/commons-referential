/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import java.util.Arrays;
import java.util.List;

import com.tinubu.commons.referential.TestData.TestDataBuilder;

public class TestReferential extends AbstractReferential<TestEntry, TestData>
      implements Referential<TestEntry> {

   public TestReferential(ReferentialCode<TestEntry, TestReferential> referentialCode,
                          EntryDataLoader<TestData> loader) {
      super(referentialCode, loader, __ -> true);
   }

   @Override
   public TestEntry loadEntry(TestData entryData) {
      return new TestEntry(this, entryData);
   }

   @Override
   public String displayName() {
      return "TEST";
   }

   public static class TestReferentialBuilder implements ReferentialProvider {

      private static final ReferentialCode<TestEntry, TestReferential> DEFAULT_REFERENTIAL_CODE =
            ReferentialCode.of("TEST");
      private static final EntryDataLoader<TestData> DEFAULT_LOADER = new EntryDataLoader<TestData>() {
         @Override
         public List<TestData> loadEntryDatas() {
            return Arrays.asList(new TestDataBuilder().entryCode("FR").entryName("France").build(),
                                 new TestDataBuilder().entryCode("US").entryName("USA").build());
         }

         @Override
         public boolean hasChanged() {
            return false;
         }
      };

      private ReferentialCode<TestEntry, TestReferential> referentialCode = DEFAULT_REFERENTIAL_CODE;
      private EntryDataLoader<TestData> entryDataLoader = DEFAULT_LOADER;

      @Override
      public ReferentialCode<TestEntry, TestReferential> referentialCode() {
         return referentialCode;
      }

      public TestReferentialBuilder referentialCode(ReferentialCode<TestEntry, TestReferential> referentialCode) {
         this.referentialCode = referentialCode;
         return this;
      }

      public TestReferentialBuilder referentialLoader(EntryDataLoader<TestData> entryDataLoader) {
         this.entryDataLoader = entryDataLoader;
         return this;
      }

      @Override
      public Class<TestReferential> referentialClass() {
         return TestReferential.class;
      }

      @Override
      public TestReferential build() {
         return new TestReferential(referentialCode, entryDataLoader);
      }
   }

}
