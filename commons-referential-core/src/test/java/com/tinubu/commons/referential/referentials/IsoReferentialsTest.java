/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials;

import static com.tinubu.commons.referential.Referentials.clearRegisteredReferentials;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.EntryCode;
import com.tinubu.commons.referential.Referentials;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Referential;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3Referential;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Numeric;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661NumericReferential;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662Referential;
import com.tinubu.commons.referential.referentials.iso31663.Iso31663Alpha4Referential;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2Referential;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Referential;
import com.tinubu.commons.test.fixture.junit.Fixture;

// FIXME Iso31663,ISO6392*, ISO6391*
public class IsoReferentialsTest extends AbstractReferentialTest {

   @BeforeAll
   public static void registerTestReferentials(@Fixture Iso6391Alpha2Referential iso6391Alpha2Referential,
                                               @Fixture Iso6392Alpha3Referential iso6392Alpha3Referential,
                                               @Fixture Iso31661Alpha2Referential iso31661Alpha2Referential,
                                               @Fixture Iso31661Alpha3Referential iso31661Alpha3Referential,
                                               @Fixture Iso31661NumericReferential iso31661NumericReferential,
                                               @Fixture Iso31662Referential iso31662Referential,
                                               @Fixture Iso31663Alpha4Referential iso31663Alpha4Referential) {
      clearRegisteredReferentials();
      Referentials.registerReferential(referentialProvider(iso6391Alpha2Referential));
      Referentials.registerReferential(referentialProvider(iso6392Alpha3Referential));
      Referentials.registerReferential(referentialProvider(iso31661Alpha2Referential));
      Referentials.registerReferential(referentialProvider(iso31661Alpha3Referential));
      Referentials.registerReferential(referentialProvider(iso31661NumericReferential));
      Referentials.registerReferential(referentialProvider(iso31662Referential));
      Referentials.registerReferential(referentialProvider(iso31663Alpha4Referential));
   }

   @Nested
   public class InstantiateIsoReferential {

      @Test
      public void testInstantiateIso31661Alpha2Referential() {
         assertThat(IsoReferentials.iso31661Alpha2Referential()).isInstanceOf(Iso31661Alpha2Referential.class);
      }

      @Test
      public void testInstantiateIso31661Alpha3Referential() {
         assertThat(IsoReferentials.iso31661Alpha3Referential()).isInstanceOf(Iso31661Alpha3Referential.class);
      }

      @Test
      public void testInstantiateIso31661NumericReferential() {
         assertThat(IsoReferentials.iso31661NumericReferential()).isInstanceOf(Iso31661NumericReferential.class);
      }

      @Test
      public void testInstantiateIso31662Referential() {
         assertThat(IsoReferentials.iso31662Referential()).isInstanceOf(Iso31662Referential.class);
      }

      @Test
      public void testInstantiateIso31663Alpha4Referential() {
         assertThat(IsoReferentials.iso31663Alpha4Referential()).isInstanceOf(Iso31663Alpha4Referential.class);
      }

      @Test
      public void testInstantiateIso6392Alpha3Referential() {
         assertThat(IsoReferentials.iso6392Alpha3Referential()).isInstanceOf(Iso6392Alpha3Referential.class);
      }

   }

   @Nested
   public class InstantiateIso31661Alpha2Entry {

      @Test
      public void testInstantiateIso31661Alpha2EntryWhenNominal() {
         assertThat(IsoReferentials.iso31661Alpha2(EntryCode.of("FR"))).isInstanceOf(Iso31661Alpha2.class);
         assertThat(IsoReferentials.iso31661Alpha2("FR")).isInstanceOf(Iso31661Alpha2.class);
      }

      @Test
      public void testInstantiateIso31661Alpha2EntryWhenBadParameters() {
         assertThatNullPointerException().isThrownBy(() -> IsoReferentials.iso31661Alpha2((EntryCode) null)).withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> IsoReferentials.iso31661Alpha2(EntryCode.of("")))
               .withMessage("'entryCode' must not be blank");
         assertThatNullPointerException().isThrownBy(() -> IsoReferentials.iso31661Alpha2((String) null)).withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException().isThrownBy(() -> IsoReferentials.iso31661Alpha2("")).withMessage("'entryCode' must not be blank");
      }

      @Test
      public void testInstantiateOptionalIso31661Alpha2EntryWhenNominal() {
         assertThat(IsoReferentials.optionalIso31661Alpha2(EntryCode.of("FR"))).isPresent().hasValueSatisfying(c -> assertThat(c).isInstanceOf(Iso31661Alpha2.class));
         assertThat(IsoReferentials.optionalIso31661Alpha2("FR")).isPresent().hasValueSatisfying(c -> assertThat(c).isInstanceOf(Iso31661Alpha2.class));
      }

      @Test
      public void testInstantiateOptionalIso31661Alpha2EntryWhenBadParameters() {
         assertThatNullPointerException()
               .isThrownBy(() -> IsoReferentials.optionalIso31661Alpha2((EntryCode) null))
               .withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> IsoReferentials.optionalIso31661Alpha2(EntryCode.of("")))
               .withMessage("'entryCode' must not be blank");
         assertThatNullPointerException()
               .isThrownBy(() -> IsoReferentials.optionalIso31661Alpha2((String) null))
               .withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException().isThrownBy(() -> IsoReferentials.optionalIso31661Alpha2("")).withMessage("'entryCode' must not be blank");
      }

   }

   @Nested
   public class InstantiateIso31661Alpha3Entry {

      @Test
      public void testInstantiateIso31661Alpha3EntryWhenNominal() {
         assertThat(IsoReferentials.iso31661Alpha3(EntryCode.of("FRA"))).isInstanceOf(Iso31661Alpha3.class);
         assertThat(IsoReferentials.iso31661Alpha3("FRA")).isInstanceOf(Iso31661Alpha3.class);
      }

      @Test
      public void testInstantiateIso31661Alpha3EntryWhenBadParameters() {
         assertThatNullPointerException().isThrownBy(() -> IsoReferentials.iso31661Alpha3((EntryCode) null)).withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> IsoReferentials.iso31661Alpha3(EntryCode.of("")))
               .withMessage("'entryCode' must not be blank");
         assertThatNullPointerException().isThrownBy(() -> IsoReferentials.iso31661Alpha3((String) null)).withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException().isThrownBy(() -> IsoReferentials.iso31661Alpha3("")).withMessage("'entryCode' must not be blank");
      }

      @Test
      public void testInstantiateOptionalIso31661Alpha3EntryWhenNominal() {
         assertThat(IsoReferentials.optionalIso31661Alpha3(EntryCode.of("FRA"))).isPresent().hasValueSatisfying(c -> assertThat(c).isInstanceOf(Iso31661Alpha3.class));
         assertThat(IsoReferentials.optionalIso31661Alpha3("FRA")).isPresent().hasValueSatisfying(c -> assertThat(c).isInstanceOf(Iso31661Alpha3.class));
      }

      @Test
      public void testInstantiateOptionalIso31661Alpha3EntryWhenBadParameters() {
         assertThatNullPointerException()
               .isThrownBy(() -> IsoReferentials.optionalIso31661Alpha3((EntryCode) null))
               .withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> IsoReferentials.optionalIso31661Alpha3(EntryCode.of("")))
               .withMessage("'entryCode' must not be blank");
      }

   }

   @Nested
   public class InstantiateIso31661NumericEntry {

      @Test
      public void testInstantiateIso31661NumericEntryWhenNominal() {
         assertThat(IsoReferentials.iso31661Numeric(EntryCode.of("250"))).isInstanceOf(Iso31661Numeric.class);
         assertThat(IsoReferentials.iso31661Numeric("250")).isInstanceOf(Iso31661Numeric.class);
      }

      @Test
      public void testInstantiateIso31661NumericEntryWhenBadParameters() {
         assertThatNullPointerException().isThrownBy(() -> IsoReferentials.iso31661Numeric((EntryCode) null)).withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> IsoReferentials.iso31661Numeric(EntryCode.of("")))
               .withMessage("'entryCode' must not be blank");
         assertThatNullPointerException().isThrownBy(() -> IsoReferentials.iso31661Numeric((String) null)).withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException().isThrownBy(() -> IsoReferentials.iso31661Numeric("")).withMessage("'entryCode' must not be blank");
      }

      @Test
      public void testInstantiateOptionalIso31661NumericEntryWhenNominal() {
         assertThat(IsoReferentials.optionalIso31661Numeric(EntryCode.of("250"))).isPresent().hasValueSatisfying(c -> assertThat(c).isInstanceOf(Iso31661Numeric.class));
         assertThat(IsoReferentials.optionalIso31661Numeric("250")).isPresent().hasValueSatisfying(c -> assertThat(c).isInstanceOf(Iso31661Numeric.class));
      }

      @Test
      public void testInstantiateOptionalIso31661NumericEntryWhenBadParameters() {
         assertThatNullPointerException()
               .isThrownBy(() -> IsoReferentials.optionalIso31661Numeric((EntryCode) null))
               .withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> IsoReferentials.optionalIso31661Numeric(EntryCode.of("")))
               .withMessage("'entryCode' must not be blank");
         assertThatNullPointerException()
               .isThrownBy(() -> IsoReferentials.optionalIso31661Numeric((String) null))
               .withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException().isThrownBy(() -> IsoReferentials.optionalIso31661Numeric("")).withMessage("'entryCode' must not be blank");
      }

   }

   @Nested
   public class InstantiateIso31662Entry {

      @Test
      public void testInstantiateIso31662EntryWhenNominal() {
         assertThat(IsoReferentials.iso31662(EntryCode.of("FR-ARA"))).isInstanceOf(Iso31662.class);
         assertThat(IsoReferentials.iso31662("FR-ARA")).isInstanceOf(Iso31662.class);
      }

      @Test
      public void testInstantiateIso31662EntryWhenBadParameters() {
         assertThatNullPointerException().isThrownBy(() -> IsoReferentials.iso31662((EntryCode) null)).withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException().isThrownBy(() -> IsoReferentials.iso31662(EntryCode.of(""))).withMessage("'entryCode' must not be blank");
         assertThatNullPointerException().isThrownBy(() -> IsoReferentials.iso31662((String) null)).withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException().isThrownBy(() -> IsoReferentials.iso31662("")).withMessage("'entryCode' must not be blank");
      }

      @Test
      public void testInstantiateOptionalIso31662EntryWhenNominal() {
         assertThat(IsoReferentials.optionalIso31662(EntryCode.of("FR-ARA"))).isPresent().hasValueSatisfying(c -> assertThat(c).isInstanceOf(Iso31662.class));
         assertThat(IsoReferentials.optionalIso31662("FR-ARA")).isPresent().hasValueSatisfying(c -> assertThat(c).isInstanceOf(Iso31662.class));
      }

      @Test
      public void testInstantiateOptionalIso31662EntryWhenBadParameters() {
         assertThatNullPointerException()
               .isThrownBy(() -> IsoReferentials.optionalIso31662((EntryCode) null))
               .withMessage("'entryCode' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> IsoReferentials.optionalIso31662(EntryCode.of("")))
               .withMessage("'entryCode' must not be blank");
      }

      @Test
      public void testInstantiateIso31662ReferentialByCountry() {
         Map<Iso31661Alpha2Code, List<Iso31662>> map = IsoReferentials.iso31662SubdivisionsByCountry();

         assertThat(map).isNotNull();
         assertThat(map.get(Iso31661Alpha2Code.FR)).allMatch(e -> e
               .country()
               .entryCode()
               .equals(Iso31661Alpha2Code.FR));
      }

      @Test
      public void testInstantiateIso31662ReferentialByCountryWhenFilter() {
         Map<Iso31661Alpha2Code, List<Iso31662>> map =
               IsoReferentials.iso31662SubdivisionsByCountry(__ -> false);

         assertThat(map).isEmpty();
      }

   }

}