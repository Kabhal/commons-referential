/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.referential.Referentials.clearRegisteredReferentials;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.referential.TestReferential.TestReferentialBuilder;

class AbstractReferentialTest {

   @BeforeAll
   public static void registerTestReferentials() {
      clearRegisteredReferentials();
      Referentials.registerReferential(new TestReferentialBuilder());
   }

   @Test
   public void testInstantiateEntryWhenNominal() {
      assertThat(testReferential().entry(EntryCode.of("FR"))).isInstanceOf(TestEntry.class);
      assertThat(testReferential().entry("FR")).isInstanceOf(TestEntry.class);
   }

   @Test
   public void testInstantiateEntryWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> testReferential().entry((EntryCode) null)).withMessage("'entryCode' must not be null");
      assertThatIllegalArgumentException().isThrownBy(() -> testReferential().entry(EntryCode.of(""))).withMessage("'entryCode' must not be blank");
      assertThatNullPointerException().isThrownBy(() -> testReferential().entry((String) null)).withMessage("'entryCode' must not be null");
      assertThatIllegalArgumentException().isThrownBy(() -> testReferential().entry("")).withMessage("'entryCode' must not be blank");
   }

   @Test
   public void testInstantiateOptionalEntryWhenNominal() {
      assertThat(testReferential().optionalEntry(EntryCode.of("FR"))).isPresent().hasValueSatisfying(c -> assertThat(c).isInstanceOf(TestEntry.class));
      assertThat(testReferential().optionalEntry("FR")).isPresent().hasValueSatisfying(c -> assertThat(c).isInstanceOf(TestEntry.class));
   }

   @Test
   public void testInstantiateOptionalEntryWhenNotFound() {
      assertThat(testReferential().optionalEntry(EntryCode.of("QL"))).isEmpty();
      assertThat(testReferential().optionalEntry("QL")).isEmpty();
   }

   @Test
   public void testInstantiateOptionalEntryWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> testReferential().optionalEntry((EntryCode) null)).withMessage("'entryCode' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> testReferential().optionalEntry(EntryCode.of("")))
            .withMessage("'entryCode' must not be blank");
      assertThatNullPointerException().isThrownBy(() -> testReferential().optionalEntry((String) null)).withMessage("'entryCode' must not be null");
      assertThatIllegalArgumentException().isThrownBy(() -> testReferential().optionalEntry("")).withMessage("'entryCode' must not be blank");
   }

   private Referential<? extends Entry<?>> testReferential() {
      return Referentials.referential(ReferentialCode.untypedOf("TEST"));
   }

}