/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Optional;

/**
 * Represents an entry in a given {@link Referential}. The entry and the referential
 * implementations are generally tied together.
 *
 * @param <E> entry type
 */
public interface Entry<E extends Entry<E>> extends Comparable<E> {

   /**
    * Entry unique code in the referential.
    *
    * @return referential unique entry code
    */
   EntryCode entryCode();

   /**
    * Returns entry "main" name as a translatable name. Most of the time, this is an alias to
    * {@link EntryData} name field for the entry. See {@link #entryData()} for other names specific to
    * each referential.
    */
   TranslatableName entryName();

   /**
    * Entry data associated with the entry. The data set, depends on the referential implementation. By
    * design, a single data implementation can be used by several referentials.
    *
    * @return entry data
    */
   EntryData entryData();

   /**
    * Links to the referential instance associated to this entry.
    *
    * @return referential associated to this entry
    */
   Referential<E> referential();

   /**
    * Compares entries by their {@link #entryCode()}.
    *
    * @param entry entry to compare to
    *
    * @return the value 0 if the argument entry's code is equal to this entry's code; a value less than 0
    *       if this entry's code is lexicographically less than the entry's code argument; and a value
    *       greater than 0 if this entry's code is lexicographically greater than the entry's code
    *       argument.
    */
   default int compareTo(E entry) {
      notNull(entry, "entry");

      return entryCode().compareTo(entry.entryCode());
   }

   /**
    * Returns an {@link Entry} instance from specified {@link ReferentialCode}, associated with this entry.
    *
    * @param referentialCode referential code to use for entry code mapping
    * @param <ME> mapped entry type
    * @param <MR> mapped referential type
    *
    * @return entry instance from specified referential, associated with this entry, or {@link Optional#empty}
    *       if no mapping found for specified referential
    */
   default <ME extends Entry<ME>, MR extends Referential<ME>> Optional<ME> entryMapping(ReferentialCode<ME, MR> referentialCode) {
      return entryData().entryCode(referentialCode).map(entryCode -> Referentials.referential(referentialCode).entry(entryCode));
   }
}
