/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso31661;

import com.tinubu.commons.referential.AbstractEntry;
import com.tinubu.commons.referential.Entry;
import com.tinubu.commons.referential.EntryData;
import com.tinubu.commons.referential.TranslatableName;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha3.Iso31661Alpha3Data;

/**
 * Represents a ISO 3166-1 alpha-3 country.
 * Each country is associated with {@link Iso31661Alpha3Data}.
 */
public class Iso31661Alpha3 extends AbstractEntry<Iso31661Alpha3Referential, Iso31661Alpha3, Iso31661Alpha3Data>
      implements Entry<Iso31661Alpha3> {

   Iso31661Alpha3(Iso31661Alpha3Referential referential, Iso31661Alpha3Data entryData) {
      super(referential, entryData);
   }

   @Override
   public Iso31661Alpha3Code entryCode() {
      return entryData.iso31661Alpha3();
   }

   @Override
   public TranslatableName entryName() {
      return entryData.name();
   }

   /**
    * Defines ISO 3166-1 alpha-3 data.
    * Most fields are optional because of user-assigned codes.
    */
   public interface Iso31661Alpha3Data extends EntryData {

      /** Entry code. */
      Iso31661Alpha3Code iso31661Alpha3();

      /** Name translations for this entry. */
      TranslatableName name();

      /** Common name translations for this entry. */
      TranslatableName commonName();

      /** Official name translations for this entry. */
      TranslatableName officialName();
   }

}
