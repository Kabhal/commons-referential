/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Represents a referential of any kind (country, monetary, ...). The referential must be used to access
 * {@link Entry entries}.
 *
 * @param <E> entry type associated to this referential
 */
public interface Referential<E extends Entry<E>> {

   /**
    * Referential unique code.
    *
    * @return Referential unique code
    */
   ReferentialCode<E, ? extends Referential<E>> referentialCode();

   /**
    * Referential official name for display. This name is fixed and is not dependent on instantiation.
    *
    * @return referential official name
    */
   String displayName();

   /**
    * Returns the whole set of {@link Entry entries} managed in the referential.
    *
    * @return the whole set of entries managed in the referential
    */
   List<E> entries();

   /**
    * Convenient function used to filter {@link #entries}.
    *
    * @param entryFilter predicate to filter entries
    *
    * @return the whole set of entries managed in the referential, filtered by the specified predicate
    */
   default List<E> entries(Predicate<E> entryFilter) {
      notNull(entryFilter, "entryFilter");

      return entries().stream().filter(entryFilter).collect(toList());
   }

   /**
    * Instantiates a {@link Entry} from this referential, by {@link EntryCode entry code}.
    *
    * @param entryCode entry code
    *
    * @return new entry instance
    *
    * @throws UnknownEntryException if entry code not found in this referential
    */
   E entry(EntryCode entryCode);

   /**
    * Instantiates a {@link Entry} from this referential, by entry code.
    *
    * @param entryCode entry code
    *
    * @return new entry instance
    *
    * @throws UnknownEntryException if entry code not found in this referential
    */
   default E entry(String entryCode) {
      return entry(EntryCode.of(entryCode));
   }

   /**
    * Instantiates a {@link Entry} from this referential, by {@link EntryCode entry code}.
    *
    * @param entryCode entry code
    *
    * @return new entry instance or {@link Optional#empty} if entry code not found in this referential
    */
   Optional<E> optionalEntry(EntryCode entryCode);

   /**
    * Instantiates a {@link Entry} from this referential, by entry code.
    *
    * @param entryCode entry code
    *
    * @return new entry instance or {@link Optional#empty} if entry code not found in this referential
    */
   default Optional<E> optionalEntry(String entryCode) {
      return optionalEntry(EntryCode.of(entryCode));
   }

}
