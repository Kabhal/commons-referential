/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso6391;

import com.tinubu.commons.referential.AbstractEntry;
import com.tinubu.commons.referential.Entry;
import com.tinubu.commons.referential.EntryCode;
import com.tinubu.commons.referential.EntryData;
import com.tinubu.commons.referential.TranslatableName;
import com.tinubu.commons.referential.referentials.iso6391.Iso6391Alpha2.Iso6391Alpha2Data;

/**
 * Represents a ISO 639-1 alpha-2 language.
 * Each country is associated with {@link Iso6391Alpha2Data}.
 */
public class Iso6391Alpha2 extends AbstractEntry<Iso6391Alpha2Referential, Iso6391Alpha2, Iso6391Alpha2Data>
      implements Entry<Iso6391Alpha2> {

   Iso6391Alpha2(Iso6391Alpha2Referential referential, Iso6391Alpha2Data entryData) {
      super(referential, entryData);
   }

   @Override
   public EntryCode entryCode() {
      return entryData
            .entryCode(referential.referentialCode())
            .orElseThrow(() -> new IllegalStateException(String.format("Required entry code for '%s' in '%s'",
                                                                       referential.referentialCode(),
                                                                       entryData)));
   }

   @Override
   public TranslatableName entryName() {
      return entryData.name();
   }

   /**
    * Defines ISO 639-1 alpha-2 data.
    * Most fields are optional because of reserved codes.
    */
   public interface Iso6391Alpha2Data extends EntryData {

      /** Name translations for this entry. */
      TranslatableName name();

   }

}
