/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import java.util.Map;
import java.util.Optional;

/**
 * Entry code mappings support for {@link EntryData}.
 */
public interface EntryCodeMapping {
   /**
    * Returns mapped entry code for specified referential
    *
    * @param referential referential code
    *
    * @return mapped entry code for specified referential or {@link Optional#empty} if no such mapping exist
    */
   Optional<EntryCode> mapping(ReferentialCode<?, ?> referential);

   /**
    * Returns registered mappings indexed by referential code.
    *
    * @return registered mappings
    */
   Map<ReferentialCode<?, ?>, EntryCode> mapping();
}
