/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential;

import java.util.Optional;

import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;

/**
 * Name with different translations.
 */
public interface TranslatableName {
   /**
    * Returns translated name for the specified language.
    *
    * @param language name language
    *
    * @return translated name for the specified language or {@link Optional#empty} if translation not found
    */
   Optional<String> name(Iso6392Alpha3Code language);

   /**
    * Delegates this translatable name to specified on in searches.
    *
    * @param delegate translatable name to delegate to in searches
    *
    * @return new translatable name with delegation enabled
    */
   TranslatableName delegate(TranslatableName delegate);
}
