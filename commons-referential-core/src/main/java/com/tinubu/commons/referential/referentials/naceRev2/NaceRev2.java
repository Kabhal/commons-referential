/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.naceRev2;

import java.util.Optional;

import com.tinubu.commons.referential.AbstractEntry;
import com.tinubu.commons.referential.Entry;
import com.tinubu.commons.referential.EntryData;
import com.tinubu.commons.referential.TranslatableName;
import com.tinubu.commons.referential.referentials.NaceReferentials;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2.NaceRev2Data;

/**
 * Represents a NACE Rev.2 activity.
 * Each activity is associated with {@link NaceRev2Data}.
 */
public class NaceRev2 extends AbstractEntry<NaceRev2Referential, NaceRev2, NaceRev2Data> implements Entry<NaceRev2> {

   NaceRev2(NaceRev2Referential referential, NaceRev2Data entryData) {
      super(referential, entryData);
   }

   @Override
   public NaceRev2Code entryCode() {
      return entryData.naceRev2();
   }

   @Override
   public TranslatableName entryName() {
      return entryData.name();
   }

   /**
    * Returns section entry for this activity.
    *
    * @return secion entry for this activity
    */
   public NaceRev2 section() {
      return NaceReferentials.naceRev2(entryData.section());
   }

   /**
    * Returns division entry for this activity.
    *
    * @return division entry for this activity or {@link Optional#empty} if entry has no division
    */
   public Optional<NaceRev2> division() {
      return entryData.division().map(NaceReferentials::naceRev2);
   }

   /**
    * Returns group entry for this activity.
    *
    * @return group entry for this activity or {@link Optional#empty} if entry has no group
    */
   public Optional<NaceRev2> group() {
      return entryData.group().map(NaceReferentials::naceRev2);
   }

   /**
    * Returns class entry for this activity.
    *
    * @return class entry for this activity or {@link Optional#empty} if entry has no class
    */
   public Optional<NaceRev2> naceClass() {
      return entryData.naceClass().map(NaceReferentials::naceRev2);
   }

   public interface NaceRev2Data extends EntryData {

      /** Entry code. */
      NaceRev2Code naceRev2();

      /** Section entry code. */
      NaceRev2Code section();

      /** Division entry code or {@link Optional#empty} if entry has no division. */
      Optional<NaceRev2Code> division();

      /** Group entry code or {@link Optional#empty} if entry has no group. */
      Optional<NaceRev2Code> group();

      /** Class entry code or {@link Optional#empty} if entry has no class. */
      Optional<NaceRev2Code> naceClass();

      /** Name translations for this entry. */
      TranslatableName name();

      /** NACE entry type. */
      NaceRev2EntryType type();
   }

   public enum NaceRev2EntryType {
      SECTION, DIVISION, GROUP, CLASS
   }
}
