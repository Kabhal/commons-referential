/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials.iso6391;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.referential.EntryCode;

/**
 * ISO 639-1 alpha-2 language code.
 * Language code format is {@code <alpha><alpha>} in lowercase (e.g.: {@code aa}).
 */
public class Iso6391Alpha2Code extends EntryCode {

   private Iso6391Alpha2Code(String entryCode) {
      super(entryCode);
   }

   /**
    * Creates ISO 639-1 alpha-2 code from entry code.
    * Entry code is case-insensitive.
    */
   public static Iso6391Alpha2Code of(String entryCode) {
      return new Iso6391Alpha2Code(notNull(entryCode, "entryCode").toLowerCase());
   }

   @Override
   protected boolean validEntryCodeFormat(String entryCode) {
      return entryCode.length() == 2 && entryCode.chars().allMatch(c -> c >= 'a' && c <= 'z');
   }

   /* Generated codes at 2021-10-12T18:10:56+02:00 using default loader. */
   public static final Iso6391Alpha2Code AA = Iso6391Alpha2Code.of("aa");
   public static final Iso6391Alpha2Code AB = Iso6391Alpha2Code.of("ab");
   public static final Iso6391Alpha2Code AE = Iso6391Alpha2Code.of("ae");
   public static final Iso6391Alpha2Code AF = Iso6391Alpha2Code.of("af");
   public static final Iso6391Alpha2Code AK = Iso6391Alpha2Code.of("ak");
   public static final Iso6391Alpha2Code AM = Iso6391Alpha2Code.of("am");
   public static final Iso6391Alpha2Code AN = Iso6391Alpha2Code.of("an");
   public static final Iso6391Alpha2Code AR = Iso6391Alpha2Code.of("ar");
   public static final Iso6391Alpha2Code AS = Iso6391Alpha2Code.of("as");
   public static final Iso6391Alpha2Code AV = Iso6391Alpha2Code.of("av");
   public static final Iso6391Alpha2Code AY = Iso6391Alpha2Code.of("ay");
   public static final Iso6391Alpha2Code AZ = Iso6391Alpha2Code.of("az");
   public static final Iso6391Alpha2Code BA = Iso6391Alpha2Code.of("ba");
   public static final Iso6391Alpha2Code BE = Iso6391Alpha2Code.of("be");
   public static final Iso6391Alpha2Code BG = Iso6391Alpha2Code.of("bg");
   public static final Iso6391Alpha2Code BH = Iso6391Alpha2Code.of("bh");
   public static final Iso6391Alpha2Code BI = Iso6391Alpha2Code.of("bi");
   public static final Iso6391Alpha2Code BM = Iso6391Alpha2Code.of("bm");
   public static final Iso6391Alpha2Code BN = Iso6391Alpha2Code.of("bn");
   public static final Iso6391Alpha2Code BO = Iso6391Alpha2Code.of("bo");
   public static final Iso6391Alpha2Code BR = Iso6391Alpha2Code.of("br");
   public static final Iso6391Alpha2Code BS = Iso6391Alpha2Code.of("bs");
   public static final Iso6391Alpha2Code CA = Iso6391Alpha2Code.of("ca");
   public static final Iso6391Alpha2Code CE = Iso6391Alpha2Code.of("ce");
   public static final Iso6391Alpha2Code CH = Iso6391Alpha2Code.of("ch");
   public static final Iso6391Alpha2Code CO = Iso6391Alpha2Code.of("co");
   public static final Iso6391Alpha2Code CR = Iso6391Alpha2Code.of("cr");
   public static final Iso6391Alpha2Code CS = Iso6391Alpha2Code.of("cs");
   public static final Iso6391Alpha2Code CU = Iso6391Alpha2Code.of("cu");
   public static final Iso6391Alpha2Code CV = Iso6391Alpha2Code.of("cv");
   public static final Iso6391Alpha2Code CY = Iso6391Alpha2Code.of("cy");
   public static final Iso6391Alpha2Code DA = Iso6391Alpha2Code.of("da");
   public static final Iso6391Alpha2Code DE = Iso6391Alpha2Code.of("de");
   public static final Iso6391Alpha2Code DV = Iso6391Alpha2Code.of("dv");
   public static final Iso6391Alpha2Code DZ = Iso6391Alpha2Code.of("dz");
   public static final Iso6391Alpha2Code EE = Iso6391Alpha2Code.of("ee");
   public static final Iso6391Alpha2Code EL = Iso6391Alpha2Code.of("el");
   public static final Iso6391Alpha2Code EN = Iso6391Alpha2Code.of("en");
   public static final Iso6391Alpha2Code EO = Iso6391Alpha2Code.of("eo");
   public static final Iso6391Alpha2Code ES = Iso6391Alpha2Code.of("es");
   public static final Iso6391Alpha2Code ET = Iso6391Alpha2Code.of("et");
   public static final Iso6391Alpha2Code EU = Iso6391Alpha2Code.of("eu");
   public static final Iso6391Alpha2Code FA = Iso6391Alpha2Code.of("fa");
   public static final Iso6391Alpha2Code FF = Iso6391Alpha2Code.of("ff");
   public static final Iso6391Alpha2Code FI = Iso6391Alpha2Code.of("fi");
   public static final Iso6391Alpha2Code FJ = Iso6391Alpha2Code.of("fj");
   public static final Iso6391Alpha2Code FO = Iso6391Alpha2Code.of("fo");
   public static final Iso6391Alpha2Code FR = Iso6391Alpha2Code.of("fr");
   public static final Iso6391Alpha2Code FY = Iso6391Alpha2Code.of("fy");
   public static final Iso6391Alpha2Code GA = Iso6391Alpha2Code.of("ga");
   public static final Iso6391Alpha2Code GD = Iso6391Alpha2Code.of("gd");
   public static final Iso6391Alpha2Code GL = Iso6391Alpha2Code.of("gl");
   public static final Iso6391Alpha2Code GN = Iso6391Alpha2Code.of("gn");
   public static final Iso6391Alpha2Code GU = Iso6391Alpha2Code.of("gu");
   public static final Iso6391Alpha2Code GV = Iso6391Alpha2Code.of("gv");
   public static final Iso6391Alpha2Code HA = Iso6391Alpha2Code.of("ha");
   public static final Iso6391Alpha2Code HE = Iso6391Alpha2Code.of("he");
   public static final Iso6391Alpha2Code HI = Iso6391Alpha2Code.of("hi");
   public static final Iso6391Alpha2Code HO = Iso6391Alpha2Code.of("ho");
   public static final Iso6391Alpha2Code HR = Iso6391Alpha2Code.of("hr");
   public static final Iso6391Alpha2Code HT = Iso6391Alpha2Code.of("ht");
   public static final Iso6391Alpha2Code HU = Iso6391Alpha2Code.of("hu");
   public static final Iso6391Alpha2Code HY = Iso6391Alpha2Code.of("hy");
   public static final Iso6391Alpha2Code HZ = Iso6391Alpha2Code.of("hz");
   public static final Iso6391Alpha2Code IA = Iso6391Alpha2Code.of("ia");
   public static final Iso6391Alpha2Code ID = Iso6391Alpha2Code.of("id");
   public static final Iso6391Alpha2Code IE = Iso6391Alpha2Code.of("ie");
   public static final Iso6391Alpha2Code IG = Iso6391Alpha2Code.of("ig");
   public static final Iso6391Alpha2Code II = Iso6391Alpha2Code.of("ii");
   public static final Iso6391Alpha2Code IK = Iso6391Alpha2Code.of("ik");
   public static final Iso6391Alpha2Code IO = Iso6391Alpha2Code.of("io");
   public static final Iso6391Alpha2Code IS = Iso6391Alpha2Code.of("is");
   public static final Iso6391Alpha2Code IT = Iso6391Alpha2Code.of("it");
   public static final Iso6391Alpha2Code IU = Iso6391Alpha2Code.of("iu");
   public static final Iso6391Alpha2Code JA = Iso6391Alpha2Code.of("ja");
   public static final Iso6391Alpha2Code JV = Iso6391Alpha2Code.of("jv");
   public static final Iso6391Alpha2Code KA = Iso6391Alpha2Code.of("ka");
   public static final Iso6391Alpha2Code KG = Iso6391Alpha2Code.of("kg");
   public static final Iso6391Alpha2Code KI = Iso6391Alpha2Code.of("ki");
   public static final Iso6391Alpha2Code KJ = Iso6391Alpha2Code.of("kj");
   public static final Iso6391Alpha2Code KK = Iso6391Alpha2Code.of("kk");
   public static final Iso6391Alpha2Code KL = Iso6391Alpha2Code.of("kl");
   public static final Iso6391Alpha2Code KM = Iso6391Alpha2Code.of("km");
   public static final Iso6391Alpha2Code KN = Iso6391Alpha2Code.of("kn");
   public static final Iso6391Alpha2Code KO = Iso6391Alpha2Code.of("ko");
   public static final Iso6391Alpha2Code KR = Iso6391Alpha2Code.of("kr");
   public static final Iso6391Alpha2Code KS = Iso6391Alpha2Code.of("ks");
   public static final Iso6391Alpha2Code KU = Iso6391Alpha2Code.of("ku");
   public static final Iso6391Alpha2Code KV = Iso6391Alpha2Code.of("kv");
   public static final Iso6391Alpha2Code KW = Iso6391Alpha2Code.of("kw");
   public static final Iso6391Alpha2Code KY = Iso6391Alpha2Code.of("ky");
   public static final Iso6391Alpha2Code LA = Iso6391Alpha2Code.of("la");
   public static final Iso6391Alpha2Code LB = Iso6391Alpha2Code.of("lb");
   public static final Iso6391Alpha2Code LG = Iso6391Alpha2Code.of("lg");
   public static final Iso6391Alpha2Code LI = Iso6391Alpha2Code.of("li");
   public static final Iso6391Alpha2Code LN = Iso6391Alpha2Code.of("ln");
   public static final Iso6391Alpha2Code LO = Iso6391Alpha2Code.of("lo");
   public static final Iso6391Alpha2Code LT = Iso6391Alpha2Code.of("lt");
   public static final Iso6391Alpha2Code LU = Iso6391Alpha2Code.of("lu");
   public static final Iso6391Alpha2Code LV = Iso6391Alpha2Code.of("lv");
   public static final Iso6391Alpha2Code MG = Iso6391Alpha2Code.of("mg");
   public static final Iso6391Alpha2Code MH = Iso6391Alpha2Code.of("mh");
   public static final Iso6391Alpha2Code MI = Iso6391Alpha2Code.of("mi");
   public static final Iso6391Alpha2Code MK = Iso6391Alpha2Code.of("mk");
   public static final Iso6391Alpha2Code ML = Iso6391Alpha2Code.of("ml");
   public static final Iso6391Alpha2Code MN = Iso6391Alpha2Code.of("mn");
   public static final Iso6391Alpha2Code MR = Iso6391Alpha2Code.of("mr");
   public static final Iso6391Alpha2Code MS = Iso6391Alpha2Code.of("ms");
   public static final Iso6391Alpha2Code MT = Iso6391Alpha2Code.of("mt");
   public static final Iso6391Alpha2Code MY = Iso6391Alpha2Code.of("my");
   public static final Iso6391Alpha2Code NA = Iso6391Alpha2Code.of("na");
   public static final Iso6391Alpha2Code NB = Iso6391Alpha2Code.of("nb");
   public static final Iso6391Alpha2Code ND = Iso6391Alpha2Code.of("nd");
   public static final Iso6391Alpha2Code NE = Iso6391Alpha2Code.of("ne");
   public static final Iso6391Alpha2Code NG = Iso6391Alpha2Code.of("ng");
   public static final Iso6391Alpha2Code NL = Iso6391Alpha2Code.of("nl");
   public static final Iso6391Alpha2Code NN = Iso6391Alpha2Code.of("nn");
   public static final Iso6391Alpha2Code NO = Iso6391Alpha2Code.of("no");
   public static final Iso6391Alpha2Code NR = Iso6391Alpha2Code.of("nr");
   public static final Iso6391Alpha2Code NV = Iso6391Alpha2Code.of("nv");
   public static final Iso6391Alpha2Code NY = Iso6391Alpha2Code.of("ny");
   public static final Iso6391Alpha2Code OC = Iso6391Alpha2Code.of("oc");
   public static final Iso6391Alpha2Code OJ = Iso6391Alpha2Code.of("oj");
   public static final Iso6391Alpha2Code OM = Iso6391Alpha2Code.of("om");
   public static final Iso6391Alpha2Code OR = Iso6391Alpha2Code.of("or");
   public static final Iso6391Alpha2Code OS = Iso6391Alpha2Code.of("os");
   public static final Iso6391Alpha2Code PA = Iso6391Alpha2Code.of("pa");
   public static final Iso6391Alpha2Code PI = Iso6391Alpha2Code.of("pi");
   public static final Iso6391Alpha2Code PL = Iso6391Alpha2Code.of("pl");
   public static final Iso6391Alpha2Code PS = Iso6391Alpha2Code.of("ps");
   public static final Iso6391Alpha2Code PT = Iso6391Alpha2Code.of("pt");
   public static final Iso6391Alpha2Code QU = Iso6391Alpha2Code.of("qu");
   public static final Iso6391Alpha2Code RM = Iso6391Alpha2Code.of("rm");
   public static final Iso6391Alpha2Code RN = Iso6391Alpha2Code.of("rn");
   public static final Iso6391Alpha2Code RO = Iso6391Alpha2Code.of("ro");
   public static final Iso6391Alpha2Code RU = Iso6391Alpha2Code.of("ru");
   public static final Iso6391Alpha2Code RW = Iso6391Alpha2Code.of("rw");
   public static final Iso6391Alpha2Code SA = Iso6391Alpha2Code.of("sa");
   public static final Iso6391Alpha2Code SC = Iso6391Alpha2Code.of("sc");
   public static final Iso6391Alpha2Code SD = Iso6391Alpha2Code.of("sd");
   public static final Iso6391Alpha2Code SE = Iso6391Alpha2Code.of("se");
   public static final Iso6391Alpha2Code SG = Iso6391Alpha2Code.of("sg");
   public static final Iso6391Alpha2Code SI = Iso6391Alpha2Code.of("si");
   public static final Iso6391Alpha2Code SK = Iso6391Alpha2Code.of("sk");
   public static final Iso6391Alpha2Code SL = Iso6391Alpha2Code.of("sl");
   public static final Iso6391Alpha2Code SM = Iso6391Alpha2Code.of("sm");
   public static final Iso6391Alpha2Code SN = Iso6391Alpha2Code.of("sn");
   public static final Iso6391Alpha2Code SO = Iso6391Alpha2Code.of("so");
   public static final Iso6391Alpha2Code SQ = Iso6391Alpha2Code.of("sq");
   public static final Iso6391Alpha2Code SR = Iso6391Alpha2Code.of("sr");
   public static final Iso6391Alpha2Code SS = Iso6391Alpha2Code.of("ss");
   public static final Iso6391Alpha2Code ST = Iso6391Alpha2Code.of("st");
   public static final Iso6391Alpha2Code SU = Iso6391Alpha2Code.of("su");
   public static final Iso6391Alpha2Code SV = Iso6391Alpha2Code.of("sv");
   public static final Iso6391Alpha2Code SW = Iso6391Alpha2Code.of("sw");
   public static final Iso6391Alpha2Code TA = Iso6391Alpha2Code.of("ta");
   public static final Iso6391Alpha2Code TE = Iso6391Alpha2Code.of("te");
   public static final Iso6391Alpha2Code TG = Iso6391Alpha2Code.of("tg");
   public static final Iso6391Alpha2Code TH = Iso6391Alpha2Code.of("th");
   public static final Iso6391Alpha2Code TI = Iso6391Alpha2Code.of("ti");
   public static final Iso6391Alpha2Code TK = Iso6391Alpha2Code.of("tk");
   public static final Iso6391Alpha2Code TL = Iso6391Alpha2Code.of("tl");
   public static final Iso6391Alpha2Code TN = Iso6391Alpha2Code.of("tn");
   public static final Iso6391Alpha2Code TO = Iso6391Alpha2Code.of("to");
   public static final Iso6391Alpha2Code TR = Iso6391Alpha2Code.of("tr");
   public static final Iso6391Alpha2Code TS = Iso6391Alpha2Code.of("ts");
   public static final Iso6391Alpha2Code TT = Iso6391Alpha2Code.of("tt");
   public static final Iso6391Alpha2Code TW = Iso6391Alpha2Code.of("tw");
   public static final Iso6391Alpha2Code TY = Iso6391Alpha2Code.of("ty");
   public static final Iso6391Alpha2Code UG = Iso6391Alpha2Code.of("ug");
   public static final Iso6391Alpha2Code UK = Iso6391Alpha2Code.of("uk");
   public static final Iso6391Alpha2Code UR = Iso6391Alpha2Code.of("ur");
   public static final Iso6391Alpha2Code UZ = Iso6391Alpha2Code.of("uz");
   public static final Iso6391Alpha2Code VE = Iso6391Alpha2Code.of("ve");
   public static final Iso6391Alpha2Code VI = Iso6391Alpha2Code.of("vi");
   public static final Iso6391Alpha2Code VO = Iso6391Alpha2Code.of("vo");
   public static final Iso6391Alpha2Code WA = Iso6391Alpha2Code.of("wa");
   public static final Iso6391Alpha2Code WO = Iso6391Alpha2Code.of("wo");
   public static final Iso6391Alpha2Code XH = Iso6391Alpha2Code.of("xh");
   public static final Iso6391Alpha2Code YI = Iso6391Alpha2Code.of("yi");
   public static final Iso6391Alpha2Code YO = Iso6391Alpha2Code.of("yo");
   public static final Iso6391Alpha2Code ZA = Iso6391Alpha2Code.of("za");
   public static final Iso6391Alpha2Code ZH = Iso6391Alpha2Code.of("zh");
   public static final Iso6391Alpha2Code ZU = Iso6391Alpha2Code.of("zu");

}


