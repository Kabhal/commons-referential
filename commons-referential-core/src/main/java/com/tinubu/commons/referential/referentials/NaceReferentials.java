/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.referentials;

import java.util.Optional;

import com.tinubu.commons.referential.EntryCode;
import com.tinubu.commons.referential.Referentials;
import com.tinubu.commons.referential.UnknownEntryException;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2Code;
import com.tinubu.commons.referential.referentials.naceRev2.NaceRev2Referential;

/**
 * Specialized fast accessors for default NACE referentials.
 */
public class NaceReferentials {

   /**
    * NACE REV2 referential accessor.
    *
    * @return {@link NaceRev2Referential} referential instance
    */
   public static NaceRev2Referential naceRev2Referential() {
      return Referentials.referential(NaceRev2Referential.DEFAULT_REFERENTIAL_CODE);
   }

   /**
    * Checked NACE Rev.2 activity accessor.
    *
    * @param activityCode activity code
    *
    * @return {@link NaceRev2} activity
    *
    * @throws UnknownEntryException if activity code is unknown in referential
    */
   public static NaceRev2 naceRev2(EntryCode activityCode) {
      return naceRev2Referential().entry(activityCode);
   }

   /**
    * Checked NACE Rev.2 activity accessor.
    *
    * @param activityCode activity code
    *
    * @return {@link NaceRev2} activity
    *
    * @throws UnknownEntryException if activity code is unknown in referential
    */
   public static NaceRev2 naceRev2(String activityCode) {
      return naceRev2(NaceRev2Code.of(activityCode));
   }

   /**
    * Optional NACE Rev.2 activity accessor.
    *
    * @param activityCode activity code
    *
    * @return {@link NaceRev2} activity or {@link Optional#empty} if activity is unknown in referential
    */
   public static Optional<NaceRev2> optionalNaceRev2(EntryCode activityCode) {
      return naceRev2Referential().optionalEntry(activityCode);
   }

   /**
    * Optional NACE Rev.2 activity accessor.
    *
    * @param activityCode activity code
    *
    * @return {@link NaceRev2} activity or {@link Optional#empty} if activity is unknown in referential
    */
   public static Optional<NaceRev2> optionalNaceRev2(String activityCode) {
      return optionalNaceRev2(NaceRev2Code.of(activityCode));
   }

}
