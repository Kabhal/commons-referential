/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.referential.loader.debian;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinubu.commons.referential.DefaultTranslatableName.DefaultTranslatableNameBuilder;
import com.tinubu.commons.referential.EntryDataLoader;
import com.tinubu.commons.referential.EntryDataLoadingException;
import com.tinubu.commons.referential.TranslatableName;
import com.tinubu.commons.referential.loader.debian.DebianIsoCodes31662ReferentialLoader.Iso31662Data.Iso31662DataBuilder;
import com.tinubu.commons.referential.referentials.iso31661.Iso31661Alpha2Code;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662;
import com.tinubu.commons.referential.referentials.iso31662.Iso31662Code;
import com.tinubu.commons.referential.referentials.iso6392.Iso6392Alpha3Code;

/**
 * ISO 3166-2 {@link EntryDataLoader} from {@value DEFAULT_JSON_RESOURCE_PATH} data file from Debian
 * project.
 */
public class DebianIsoCodes31662ReferentialLoader implements EntryDataLoader<Iso31662.Iso31662Data> {

   private static final ObjectMapper OBJECT_MAPPER = objectMapper();

   private static final String DEFAULT_JSON_RESOURCE_PATH = "resources/iso_3166-2.json";

   /** JSON key used in Debian iso-codes definitions. */
   private static final String JSON_CONTENT_KEY = "3166-2";

   private final List<Iso31662Data> entryDatas;

   private DebianIsoCodes31662ReferentialLoader(List<Iso31662Data> entryDatas) {
      this.entryDatas = noNullElements(entryDatas, "entryDatas");
   }

   /**
    * Instantiates loader from {@value DEFAULT_JSON_RESOURCE_PATH} provided data file.
    *
    * @return loader instance
    */
   public static DebianIsoCodes31662ReferentialLoader fromDefaultSource() {
      return new DebianIsoCodes31662ReferentialLoader(loadDataFromDefaultSource());
   }

   /**
    * Instantiates loader from user-provided data source.
    *
    * @return loader instance
    */
   public static DebianIsoCodes31662ReferentialLoader fromSource(InputStream source) {
      notNull(source, "source");

      return new DebianIsoCodes31662ReferentialLoader(loadDataFromSource(source));
   }

   private static List<Iso31662Data> loadDataFromDefaultSource() {
      try (InputStream jsonStream = jsonContentStream()) {
         return loadDataFromSource(jsonStream);
      } catch (IOException e) {
         throw new EntryDataLoadingException(e.getMessage(), e);
      }
   }

   private static List<Iso31662Data> loadDataFromSource(InputStream source) {
      return parseJsonContent(source)
            .get(JSON_CONTENT_KEY)
            .stream()
            .map(DebianIsoCodes31662ReferentialLoader::entryData)
            .collect(toList());
   }

   private static Iso31662Data entryData(JsonData json) {
      Iso31662Code entryCode = Iso31662Code.of(json.iso31662);
      Iso31661Alpha2Code countryCode = countryCode(entryCode);

      return new Iso31662DataBuilder()
            .iso31662(entryCode)
            .country(countryCode)
            .name(new DefaultTranslatableNameBuilder().name(Iso6392Alpha3Code.ENG, json.name).build())
            .type(json.type)
            .parentEntry(parentCode(countryCode, json.parent))
            .build();
   }

   private static Iso31662Code parentCode(Iso31661Alpha2Code countryCode, String parentCode) {
      return nullable(parentCode).map(pc -> Iso31662Code.of(countryCode.entryCode() + "-" + pc)).orElse(null);
   }

   private static Iso31661Alpha2Code countryCode(Iso31662Code code) {
      return Iso31661Alpha2Code.of(code.entryCode().split("-")[0]);
   }

   @Override
   public List<Iso31662.Iso31662Data> loadEntryDatas() {
      try {
         return entryDatas.stream().map(Iso31662.Iso31662Data.class::cast).collect(toList());
      } catch (EntryDataLoadingException e) {
         throw e;
      } catch (Exception e) {
         throw new EntryDataLoadingException(e.getMessage(), e);
      }

   }

   @Override
   public boolean hasChanged() {
      return false;
   }

   private static Map<String, List<JsonData>> parseJsonContent(InputStream source) {
      try {
         return OBJECT_MAPPER.readValue(source, new TypeReference<Map<String, List<JsonData>>>() {});
      } catch (IOException e) {
         throw new EntryDataLoadingException(e.getMessage(), e);
      }
   }

   private static InputStream jsonContentStream() {
      return DebianIsoCodes31662ReferentialLoader.class.getResourceAsStream(DEFAULT_JSON_RESOURCE_PATH);
   }

   private static ObjectMapper objectMapper() {
      return new ObjectMapper();
   }

   @JsonIgnoreProperties(ignoreUnknown = true)
   private static class JsonData {
      private String iso31662;
      private String name;
      private String type;
      private String parent;

      @JsonProperty("code")
      public String iso31662() {
         return iso31662;
      }

      @JsonProperty("name")
      public String name() {
         return name;
      }

      @JsonProperty("type")
      public String type() {
         return type;
      }

      @JsonProperty("parent")
      public String parent() {
         return parent;
      }
   }

   static class Iso31662Data implements Iso31662.Iso31662Data {
      private final Iso31662Code iso31662;
      private final Iso31662Code parent;
      private final Iso31661Alpha2Code country;
      private final TranslatableName name;
      private final String type;

      private Iso31662Data(Iso31662DataBuilder builder) {
         this.iso31662 = notNull(builder.iso31662, "iso31662");
         this.country = notNull(builder.country, "country");
         this.name = notNull(builder.name, "name");
         this.type = notBlank(builder.type, "type");
         this.parent = nullable(builder.parent, parent -> notNull(parent, "parent"));
      }

      @Override
      public Iso31662Code iso31662() {
         return iso31662;
      }

      @Override
      public Iso31661Alpha2Code country() {
         return country;
      }

      @Override
      public TranslatableName name() {
         return name;
      }

      @Override
      public String type() {
         return type;
      }

      @Override
      public Optional<Iso31662Code> parent() {
         return nullable(parent);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", Iso31662Data.class.getSimpleName() + "[", "]")
               .add("iso31662='" + iso31662 + "'")
               .add("country='" + country + "'")
               .add("name='" + name + "'")
               .add("type='" + type + "'")
               .add("parent='" + parent + "'")
               .toString();
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         Iso31662Data that = (Iso31662Data) o;
         return Objects.equals(iso31662, that.iso31662)
                && Objects.equals(country, that.country)
                && Objects.equals(name, that.name)
                && Objects.equals(type, that.type)
                && Objects.equals(parent, that.parent);
      }

      @Override
      public int hashCode() {
         return Objects.hash(iso31662, country, name, type, parent);
      }

      static class Iso31662DataBuilder {
         private Iso31662Code iso31662;
         private Iso31661Alpha2Code country;
         private TranslatableName name = new DefaultTranslatableNameBuilder().build();
         private String type;
         private Iso31662Code parent;

         public Iso31662DataBuilder iso31662(Iso31662Code iso31662) {
            this.iso31662 = iso31662;
            return this;
         }

         public Iso31662DataBuilder country(Iso31661Alpha2Code country) {
            this.country = country;
            return this;
         }

         public Iso31662DataBuilder name(TranslatableName name) {
            this.name = name;
            return this;
         }

         public Iso31662DataBuilder type(String type) {
            this.type = type;
            return this;
         }

         public Iso31662DataBuilder parentEntry(Iso31662Code parentEntry) {
            this.parent = parentEntry;
            return this;
         }

         public Iso31662Data build() {
            return new Iso31662Data(this);
         }
      }
   }
}
