#!/bin/bash

set -eo pipefail
exec 3>&1 # Create FD 3 to output non-capturing text using 1>&3

test -z "$MVN" && MVN="mvn"
test -z "$GIT" && GIT="git"
test -z "$KEEP_BRANCH" && KEEP_BRANCH=false
test -z "$PUSH" && PUSH=true
test -z "$LOG_FILE" && LOG_FILE=".release.log"

test -z "$COMMAND" && COMMAND="$1"
test -z "$VERSION" && VERSION="$2"
test -z "$RESUME_STEP" && { test ! -z "$3" && RESUME_STEP="$3" || RESUME_STEP=0; }

main() {
  test -z "$COMMAND" && fatal_error 1 "No COMMAND set"
  test -z "$VERSION" && fatal_error 1 "No VERSION set"

  title "$COMMAND $VERSION"

  trap trap_on_process_exit_before_push EXIT

  step git_flow_init "Initialize GIT flow"
  step sync_repositories "Synchronize GIT repository"

  case "$COMMAND" in
  "release")
    step start_release "Start release"
    step upgrade_release_version "Upgrade project release version"
    step test_release "Test release"
    step finish_release "Finish release"
    step upgrade_develop_version "Upgrade project development version"
    ;;
  "hotfix")
    step start_hotfix "Start hotfix"
    step upgrade_release_version "Upgrade project hotfix version"
    step test_release "Test release"
    step finish_hotfix "Finish hotfix"
    ;;
  "support")
    step start_support "Start support"
    step upgrade_release_version "Upgrade project support version"
    step test_release "Test release"
    step finish_support "Finish support"
    ;;
  *) fatal_error 1 "Unknown '$COMMAND' command" ;;
  esac

  trap trap_on_process_exit EXIT

  step push_modifications "Push all modifications to repository"

  trap EXIT
}

# push_modifications step logs won't be synchronized to not let uncommitted changes in working tree
trap_on_process_exit_before_push() {
  synchronize_logs
  trap_on_process_exit
}

trap_on_process_exit() {
  current_step=$(current_step)

  text ""
  error "The process has not been completed. You can fix the issue and resume the process from step $current_step using '$0 $COMMAND $VERSION $current_step'"
}

git_flow_init() {
  if ! $GIT config --get gitflow.branch.master &>/dev/null; then
    info "Git flow not initialized, starting initialization"
    command $GIT config --local gitflow.branch.master "releases"
    command $GIT config --local gitflow.branch.develop "master"
    command $GIT config --local gitflow.prefix.feature "feature/"
    command $GIT config --local gitflow.prefix.release "release/"
    command $GIT config --local gitflow.prefix.hotfix "hotfix/"
    command $GIT config --local gitflow.prefix.support "support/"
    command $GIT config --local gitflow.prefix.versiontag "release-"
  fi
}

sync_repositories() {
  master_branch="$($GIT config --get gitflow.branch.master)"
  develop_branch="$($GIT config --get gitflow.branch.develop)"

  command $GIT fetch origin ${master_branch}:${master_branch} --update-head-ok
  command $GIT fetch origin ${develop_branch}:${develop_branch} --update-head-ok
}

start_release() {
  command $GIT flow release start "$VERSION"
}

start_hotfix() {
  versiontag="$($GIT config --get gitflow.prefix.versiontag)"
  hotfix_base="${versiontag}$(hotfix_base_version "$VERSION")"

  next_version="$(next_minor_version "$(hotfix_base_version "$VERSION")")"
  if ! test -z "$(git tag -l "${versiontag}${next_version}")"; then
    fatal_error 2 "A new release has been detected : ${next_version}. Hotfix is for LAST RELEASED VERSION ONLY, for previous releases, use a support command"
  fi

  command $GIT flow hotfix start "$VERSION" "$hotfix_base"
  question "Please patch this project"
}

start_support() {
  versiontag="$($GIT config --get gitflow.prefix.versiontag)"
  support_base="${versiontag}$(hotfix_base_version "$VERSION")"

  next_version="$(next_minor_version "$(hotfix_base_version "$VERSION")")"
  if test -z "$(git tag -l "${versiontag}${next_version}")"; then
    warning "No new release has been detected"
    question "You could use a 'hotfix' command" "Ctrl-C to break, or hit ENTER to continue with support branch"
  fi

  command $GIT flow support start "$VERSION" "$support_base"
  question "Please patch this project"
}

upgrade_release_version() {
  newVersion "$VERSION"
}

test_release() {
  while true; do
    if snapshots=$(find . -name "pom.xml" -print0 | xargs -0 grep -Hn -- '-SNAPSHOT'); then
      warning "SNAPSHOT dependencies found :"
      output echo "$snapshots"
      question "Please update pom.xml for remaining -SNAPSHOT dependencies"
    fi
    if command $MVN -U clean test -Dorg.slf4j.simpleLogger.defaultLogLevel=warn; then
      break
    else
      question "Please fix test"
    fi
  done
  question "Last chance to update/check release before finish"
  command_sync $GIT commit -a -m "$COMMAND: Upgrade version to $VERSION"
}

finish_release() {
  versiontag="$($GIT config --get gitflow.prefix.versiontag)"

  while ! command $GIT flow release finish -m "${versiontag}${VERSION}" $($KEEP_BRANCH && echo "-k") "$VERSION"; do
    question "Resolve conflicts"
    fix_unchanged_conflict_resolution
    command_sync $GIT commit -a --no-edit
  done
}

finish_hotfix() {
  versiontag="$($GIT config --get gitflow.prefix.versiontag)"

  while ! command $GIT flow hotfix finish -m "${versiontag}${VERSION}" $($KEEP_BRANCH && echo "-k") "$VERSION"; do
    question "Resolve conflicts"
    fix_unchanged_conflict_resolution
    command_sync $GIT commit -a --no-edit
  done
}

finish_support() {
  develop="$($GIT config --get gitflow.branch.develop)"
  versiontag="$($GIT config --get gitflow.prefix.versiontag)"

  git tag "${versiontag}${VERSION}"
  git checkout "$develop"
}

# If, after resolving a conflict, no files are changed in the local branch, executing again git flow will
# regenerate the conflicts indefinitely. If we detect this case, we generate an artificial change to commit.
fix_unchanged_conflict_resolution() {
  if $GIT diff-index --quiet HEAD; then
    warning "No file updated after conflict resolution, create an arbitrary change to prevent finish operation loop"
    log "Conflict resolution without change, generating an artificial change to create a commit"
  fi
}

upgrade_develop_version() {
  next_version="$(next_development_version "$VERSION")"

  newVersion "${next_version}"
  command_sync $GIT commit -a -m "$COMMAND: Upgrade version to ${next_version}"
}

push_modifications() {
  if $PUSH; then
    question "Do you want to push modifications" "Ctrl-C to break, or hit ENTER to push modifications"
    case "$COMMAND" in
    "support")
      command $GIT push -u origin --tags "support/$VERSION"
      ;;
    *)
      command $GIT push -u origin --tags releases
      command $GIT push -u origin master
      ;;
    esac
  else
    warning "Automatic push disabled, proceed manually (do not forget to push tags on releases branch)"
  fi
}

step() {
  func="$1"
  name="$2"
  shift 2

  test -z "$auto_step" && export auto_step=0

  if test $RESUME_STEP -le $auto_step; then
    title "Step $auto_step - $name"
    $func "$@"
  fi

  auto_step=$((auto_step + 1))
}

current_step() {
  test -z "$auto_step" && echo 0 || echo $auto_step
}

newVersion() {
  command $MVN versions:set -DnewVersion="$1"
}

# Return next minor version for the given version
# Next development version : MAJOR.MINOR+1.0
next_minor_version() {
  version="$1"

  major="$(echo "$version" | cut -d . -f 1)"
  minor="$(echo "$version" | cut -d . -f 2)"
  patch="$(echo "$version" | cut -d . -f 3)"

  { test -z "$major" || test -z "$minor" || test -z "$patch"; } && fatal_error 1 "Bad '$version' format"

  echo "${major}.$((minor + 1)).0"
}

# Return development next SNAPSHOT version for the given release version
# Next development version : MAJOR.MINOR+1.0-SNAPSHOT
next_development_version() {
  version="$1"

  echo "$(next_minor_version "$version")-SNAPSHOT"
}

# Return hotfix base version for the given hotfix version
# Hotfix base version : MAJOR.MINOR.PATCH-1
hotfix_base_version() {
  version="$1"

  major="$(echo "$version" | cut -d . -f 1)"
  minor="$(echo "$version" | cut -d . -f 2)"
  patch="$(echo "$version" | cut -d . -f 3)"

  { test -z "$major" || test -z "$minor" || test -z "$patch"; } && fatal_error 1 "Bad '$version' format"
  test $patch -le 0 && fatal_error 1 "Hotfix '$version' version patch number must be > 0"
  echo "${major}.${minor}.$((patch - 1))"
}

# Execute a command with extra formatting and logging.
command() {
  output setx "$@" && errCode=$? || errCode=$?
  log "[command] $* -> $errCode"
  return ${errCode}
}

# Execute a command with extra formatting and logging. Log command and synchronize logs before executing the command.
command_sync() {
  log "[command] $*"
  synchronize_logs
  output setx "$@" && errCode=$? || errCode=$?
  return ${errCode}
}

# Logs are output to an Out-of-working-tree log file to prevent working tree to be changed while working.
# Use synchronize_logs to resync logs in a controlled manner
log() {
  { test -z "$oowt_log_file" || test ! -f "$oowt_log_file"; } && {
    oowt_log_file="$(mktemp)"
    export oowt_log_file
  }

  echo "$(date_iso8601) [$$] [$COMMAND $VERSION] $*" >>"$oowt_log_file"
}

# Synchronize Out-of-working-tree log file content to working-tree log file.
# Oowt log file is deleted in the operation.
synchronize_logs() {
  test -z "$oowt_log_file" && fatal_error 1 "No oowt log file set"

  if test -f "$oowt_log_file"; then
    if ! test -f "$LOG_FILE"; then
      touch "$LOG_FILE"
      $GIT add "$LOG_FILE"
    fi

    cat "$oowt_log_file" >>"$LOG_FILE"
    rm -f "$oowt_log_file"
  fi
}

question() {
  question="$1"
  test -z "$2" && actions="then hit ENTER" || actions="$2"
  read -p "$(color 36 "$question - $actions")" 1>&3
}

color() {
  color="$1"
  shift 1

  echo -e "\033[${color}m$*\033[m"
}

date_iso8601() {
  case "$(uname -s)" in
  "Darwin") date -u +"%Y-%m-%dT%H:%M:%SZ" ;;
  *) date -Iseconds ;;
  esac
}

seds() {
  sed "s/$1/$2/$3"
}

prefix() {
  prefix="$1"
  seds '^' "$prefix" 'g'
}

output() {
  "$@" 2>&1 | prefix "$(color 34 "|") " 1>&3
  errCode=$?
  return $errCode
}

text() {
  echo "$@" 1>&3
}

warning() {
  echo "$@" | prefix "[$(color '33;1' "W")] " 1>&3
}

error() {
  echo "$@" | prefix "[$(color '31;1' "E")] " 1>&3
}

fatal_error() {
  errCode="$1"
  shift 1
  error "$@"
  log "[E] $* -> $errCode"
  exit $errCode
}

title() {
  color '32;1' "$*" 1>&3
}

info() {
  echo "$*" 1>&3
}

setx() {
  set -x
  "$@"
  {
    errCode=$?
    set +x
  } 2>/dev/null
  return ${errCode}
}

main "$@"
